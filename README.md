# GroupAdmin Bot

A Telegram bot to manage the [@AnimuxOwO](https://t.me/AnimuxOwO) group.

# License

GroupAdmin Bot is licensed under the [GNU General Public License v3](LICENSE).
